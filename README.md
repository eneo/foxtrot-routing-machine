# FOXTROT ROUTING MACHINE
Use Foxtrot to generate personalized itineraries for walking or cycling, depending on your mood and what you want to discover during your daily walks : nature, tourism, local agriculture...
Foxtrot is developed and maintained by [ENEO](https://eneo.fr). It has been financed by Orleans Metropole and Interreg program.
It is based on [Open Source Routing Machine (OSRM)](https://project-osrm.org)
and [Leaflet Routing Machine](https://github.com/perliedman/leaflet-routing-machine)
and [Express](http://expressjs.com/)
and use [Osmium](https://osmcode.org/osmium-tool/) for some steps.
Other inspiration : [Dockerize nginx and certbot](https://github.com/wmnnd/nginx-certbot)
Other tutorial : https://engineering.door2door.io/tutorial-routing-with-osrm-and-node-js-646060489b4c

## LICENCES
see LICENCE.md

## REQUIREMENTS
Installation of _Docker_ & _docker compose_:
* Mac OSX via homebrew: `brew cask install docker`
* others: [https://docs.docker.com/engine/installation/](https://docs.docker.com/engine/installation/)
Installation of _git_
Optional : use of _vscode_

## ARCHITECTURE AND TOOLS
* osmium : extract data from OSM into a *.osm.pbf file
* imposm : import OSM data (zones and points) from .pbf file into a database
* osrm : use linear data (network) from .pbf file to generate routable graph. Use db data (zones and points) to apply weights on linears. Generate multiple files `*.osrm.*`.
* db : contains OSM data, other open data and users and statistics data
* backend : node express server. Use generated osrm graphs.
* frontend : display leaflet map. Use as a debug map (to show weights of linear) or as a user-friendly UI to show itineraries
* proxy : nginx and certbot to deploy the services on a server with ssl management

# INSTALL

## STEP 1 : EXTRACT OSM DATA FOR AREA OF INTEREST - EXAMPLE WITH ORLEANS CITY (FRANCE)

### via OSM export for small areas
OSM Export: https://www.openstreetmap.org/export

### via Geofabrik and osmium tool for larger areas
* download data from geofabrik (http://download.geofabrik.de/europe/france/centre-latest.osm.pbf) or openstreetmap (http://download.openstreetmap.fr/extracts/europe/france/centre-latest.osm.pbf)
`curl --create-dirs --output data/temp/centre-latest.osm.pbf http://download.geofabrik.de/europe/france/centre-latest.osm.pbf`
* build osmium image
`docker build . -t osmium:latest -f osmium.Dockerfile`
* you can extract an specific area if needed from .pbf file
  - create folder data/area if it does not exists
  - duplicate osmium.example.poly to data/area/area.poly and modify coordinates of area of interest in area.poly
  - extract data for this area :
`docker run --rm -v "$(pwd)"/data:/data osmium osmium extract /data/temp/centre-latest.osm.pbf --output /data/area/area_no_loc.osm.pbf --polygon /data/area/area.poly --overwrite --verbose`
* get ways data (to use them in profiles) :
`docker run --rm -v "$(pwd)"/data:/data osmium osmium add-locations-to-ways /data/area/area_no_loc.osm.pbf --output /data/area/area.osm.pbf -n --overwrite --verbose`
* if problem of memory you must create the previous files on another server or computer then upload the file on the server.
`scp data/area/area.osm.pbf <user>@<server>:<path>/data/area`

## STEP 2 : INIT DB
* copy .env.example to .env and modify POSTGRES CONFIG
* init DB (container is created and init scripts are triggered)
`docker-compose up -d db`

## STEP 3 : IMPORT ZONES AND POINTS FROM OSM INTO DB WITH IMPOSM
* if you already have osm data in db, please delete them with
`docker-compose exec db psql -U <postgres_user> -d <postgres_db> -f /tools/drop_osm_tables.sql`
* import data from area.osm.pbf
`docker run --rm --network="foxtrot_net" -v "$(pwd)"/data:/data -v "$(pwd)"/imposm-mapping.yaml:/imposm-mapping.yaml jawg/imposm3 import -mapping /imposm-mapping.yaml -read /data/area/area.osm.pbf -overwritecache -write -connection postgis://<postgres_user>:<postgres_password>@<db_host>/<postgres_db>`

## STEP 4 : IMPORT OPENDATA SQL FILES INTO DB
* (optional) extract data from local postgres DB and 
`docker exec docker_postgres_1 pg_dump -U postgres -t <schema.table> <postgres_db> > data/temp/<table_dump>.sql`
* upload SQL files to server
`scp opendata.sql <user>@<server>:<path>/data/temp`
* import data into db
if field contains multiple words they are separated with _ (ex: common_name)
`docker-compose exec db psql -U <postgres_user> -d <postgres_db> -f /data/temp/opendata.sql`

## STEP 5 : IMPORT "DATATOURISME" DATA FROM WEBSERVCE
### One time
* optional : import local zip on server and unzip
`scp <datatourisme-flux.zip> <user>@<server>:<path>/data/temp/<datatourisme-flux.zip>`
`unzip /data/temp/<datatourisme-flux.zip>`
* in .env set DATATOURISME_SITE_URL and DATATOURISME_EVENT_URL
* run import script. Parameters :
 - --tag : name of the tag to add to data (tag). Must be : site OR event (mandatory)
 - --zipfilepath : in case import from zipfile. Otherwise it downloads from webservice set in .env
 - --pghost --pgdb --pguser --pgpassword : credentials to postgres DB. Optional, default : the ones set in env file
`docker-compose exec db node /tools/loadDatatourismeToDatabase.js --tag= --zipfilepath= --pghost= --pgdb= --pguser= --pgpassword= `
### Regular update with cron
Use [cron generator](https://crontab-generator.org/) to create your own cron.
ex : `* * * * 1 docker-compose -f docker-compose.yml exec db node /tools/loadDatatourismeToDatabase.js -tag=event >> /tools/loadDatatourismeToDB.log` will execute every monday and output log will be added in loadDatatourismeToDB.log

## STEP 6 : GENERATE OSRM ROUTABLE GRAPH FOR A PROFILE : EXAMPLE WITH "GREEN-FOOT" PROFILE
* build osrm image. Osrm image is only used to generate osrm graph or to debug.
`docker build ./osrm -t osrm:latest -f ./osrm/Dockerfile`
* start db (needed to get data from osm)
`docker-compose up -d db`
* create folder for graph data and copy pbf file in it
`mkdir data/area/osrm-green-foot`
`cp data/area/area.osm.pbf data/area/osrm-green-foot/area.osm.pbf`
* create graph : osrm graph is generated in the same folder as pbf file, with same name (ex: area.osm.pbf => area.osrm). .env file is provided for access to DB.
`docker run --rm --network="foxtrot_net" -v "$(pwd)"/data:/data --env-file .env -e PROFILE='/profiles/green-foot.lua' -e PBF='/data/area/osrm-green-foot/area.osm.pbf' -e GRAPH='/data/area/osrm-green-foot/area.osrm' osrm /create-graph.sh`

## STEP 7 : API KEYS AND .ENV
* Edit .env and set `APP_SECRET` with ever key you want.
* You must add a new api key for the frontend, as if it was a user (see below "User and api management")
* Add the frontend new api key in .env var `FRONTEND_APIKEY`
* Set other .env variables correctly, depending on your installation.

## STEP 8 (OPTIONAL): CONFIGURATION OF A CHATBOT
It is possible to request routes from a chatbot created in botnation.io.
* You must add the parameter sender=chatbot in the query sent from the chatbot.
* You must add a new api key for the chatbot, as if it was a user (see below "User and api management")
* Add the new api key in .env var `CHATBOT_APIKEY`
When you request foxtrot for routes, it answers with a json response which create a carousel view in the chatbot.

# ADDITIONAL STEPS FOR PRODUCTION

## CONFIGURE PROXY AND CERTBOT
* Duplicate proxy/init-letsencrypt.example.sh to proxy/init-letsencrypt.sh
* Duplicate proxy/nginx/app.example.conf to proxy/nginx/app.conf
* Add domains and email addresses to init-letsencrypt.sh
* Replace all occurrences of example.org with primary domain (the first one you added to init-letsencrypt.sh) in proxy/nginx/app.conf
* Run `chmod +x init-letsencrypt.sh` and `sudo ./init-letsencrypt.sh`

# START SERVICES FOR PRODUCTION
`docker-compose up -d`

# USER AND API KEY MANAGEMENT
Only a person who knows the value of APP_SECRET can administrate users.
The main url is `/auth`, ex: `http://foxtrot.eneo.fr/auth`
For any request you must add `APP_SECRET` as a header of type `API key` named `api-key`
* GET request gives the list of registered users who can access the service (who have an api key)
* POST request creates a new user. You must provide at least the `name` and `email`
* PUT request modifies the user. You must provide `email` to modify the user.

# STEPS FOR DEVELOPMENT

## START DB CONTAINER
`docker-compose -f docker-compose.dev.yml up -d`

## START BACKEND
You need to use node 10.
`cd backend`
`npm install`
__DEBUG session__
Open VSCode > go to Run & Debug tab > Select the wanted configuration (backend) > Run it.
Place breakpoint wherever you want in the code directly in vscode and see the browser console in vscode.
__NORMAL session__
`npm run dev`

## START FRONTEND
`cd frontend`
`npm install`
`npm run dev`

## ACCESS APP AND API ENDPOINTS
__backend__ : http://localhost:80
__frontend__ : http://localhost:9966
* Ex of request: `node test/test_express_server.js`
* `GET /health`: A simple ping endpoint to check that the server is running.
* `GET /route`: Implements calls to `osrm.route` to calculate the way from A to B.
  _Example body_:
  ```
    { coordinates: [[13.3905, 52.5205], [13.3906, 52.5206]] }
  ```
* `POST /table`: Implements calls to `osrm.table` to get a travel times matrix for all provided locations.
  _Example body_:
  ```
    { coordinates: [[13.3905, 52.5205], [13.3906, 52.5206]] }
  ```
* `/map/`: frontend
* frontend debug (to see data and weights) :
http://localhost/map/debug/#12.52/47.8930/1.9061
* FYI the 'test' folder contains a response of the following request :
`curl 'http://router.project-osrm.org/route/v1/driving/13.388860,52.517037;13.428555,52.523219?alternatives=3&steps=true&geometries=geojson&overview=simplified&annotations=true'`

## DEBUG AN OSRM GRAPH
* start an osrm instance with the specific osrm graph (that you have generated earlier).
`docker run --rm --network="foxtrot_net" -v "$(pwd)"/data:/data -v "$(pwd)"/osrm/profiles:/profiles --env-file .env -e OSRM_GRAPH=/data/area/osrm-green-foot/area.osrm -p 5000:5000 osrm`
* to see the rates of the profile, use the debug map from frontend. You have to modify source url of tiles to display in frontend/debug/index.html (L138)

## HOW TO ADD A NEW PROFILE
FYI multiple profiles already exist in osrm/profiles.
* create a file <profile-name>.lua in osrm/profiles. You can duplicate an existing one. You must name it this way : theme-mode.lua (ex: tourism-foot.lua)
* create a file <profile-theme>-coeffs.lua in osrm/profiles/coeffs (ex: tourism-coeffs.lua). It contains OSM tags and other sources tags by geometry types and associated values (weights/rates).
* in lib/way_handlers.lua, function "rates", add setRates functions with good parameters. If you have to create new type of request, add it in "rates.lua" file.
* generate osrm routable graph as explained before.
* add new argument in .env file. Must begin with `OSRM_GRAPH_`
* if the profile contains POIs to display on frontend, you have to add a json config file in backend/src/config/pois and add the tags in backend/src/controllers/poiController.js

# TODO README
* how to create a backup.sql file
