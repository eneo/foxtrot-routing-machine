const fs = require('fs');
const fse = require('fs-extra');
const axios = require('axios');
const extract = require('extract-zip')
const glob = require("glob-promise");
var path = require('path');
const Pool = require('pg').Pool;
const args = require('yargs').argv;
require('log-timestamp');


const folder = "/objects/";
const fields = ["schema:endDate", "schema:startDate","rdfs:comment", 'hasContact', 'hasDescription','isLocatedAt',"rdfs:label","hasRepresentation",'lastUpdate'];
let directory = "/data/temp/datatourisme";

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

async function deleteTempDir(tempDir){
  try {
    fse.removeSync(path.resolve(tempDir), err => {
      if (err) {
        console.log(err)
        errors.push(err.message)
      } else {
        console.log("old folders deleted");
      }
    });
  } catch (error) {
    console.log(error)
  }
}

async function downloadFile(tag, directory) {
  if (!fs.existsSync(path.resolve(directory))){
    fs.mkdirSync(path.resolve(directory), { recursive: true });
  }
  const zipfilename = tag + '.zip';
  zipfilepath = path.resolve(path.join(directory, zipfilename))
  const writer = fs.createWriteStream(zipfilepath);
  const url = tag === 'site' ? process.env.DATATOURISME_SITE_URL : process.env.DATATOURISME_EVENT_URL

  return axios({
    method: 'get',
    url: url,
    responseType: 'stream',
  }).then(response => {
    //ensure that the user can call `then()` only when the file has
    //been downloaded entirely.
    return new Promise((resolve, reject) => {
      response.data.pipe(writer);
      let error = null;
      writer.on('error', err => {
        error = err;
        writer.close();
        reject(err);
      });
      writer.on('close', () => {
        if (!error) {
          console.log('DONE download ' + url + ' into ' + zipfilepath)
          resolve(true);
        }
        //no need to call the reject here, as it will have been called in the
        //'error' stream;
      });
    });
  });
}


(async () => {
  try {
    const tag = args.tag;
    let zipfilepath = args.zipfilepath;
    const host = args.pghost || process.env.POSTGRES_HOST;
    const db = args.pgdb || process.env.POSTGRES_DB;
    const pguser = args.pguser || process.env.POSTGRES_USER;
    const pgpassword = args.pgpassword || process.env.POSTGRES_PASSWORD;

    if (!tag || !host || !db || !pguser || !pgpassword) {
      console.log('Usage: node loadDatatourismeToDatabase.js --tag= --zipfilepath= --pghost= --pgdb= --pguser= --pgpassword= ');
      process.exit(1);
    }

    if (!['site','event'].includes(tag)) {
      console.log("tag value must be either: 'site' or 'event'");
      process.exit(1);
    }
    
    const pool = new Pool({
      user: pguser,
      host: host,
      database: db,
      password: pgpassword,
      port: "5432",
    });
    console.log('connected to pg database', pool.options.database)

    directory = path.join(directory, tag)
    // remove previously downloaded files and directories
    await deleteTempDir(directory)
    // download zip file from datatourisme.fr
    if (!zipfilepath)
      await downloadFile(tag, directory);
    else
      console.log('Zipfile used : ' + zipfilepath)
    // extract zip
    await extract(zipfilepath, { dir: directory })
    console.log('DONE extract ' + tag)

    // remove old table
    await pool.query('DROP TABLE IF EXISTS import.dt_poi_old;')
    // save current table as old
    await pool.query('ALTER TABLE IF EXISTS import.dt_poi RENAME TO dt_poi_old;') 
    // create new empty table
    await pool.query('CREATE TABLE import.dt_poi ( id SERIAL PRIMARY KEY, name VARCHAR(150), email VARCHAR(50), phone VARCHAR(50), web VARCHAR(150), startdate DATE, enddate DATE, comment TEXT, description TEXT, pictures VARCHAR(250)[], tag VARCHAR(50), last_update DATE, geom GEOMETRY (Point,3857));')   
    
    // get extracted files path
    const files = await glob.promise(path.resolve(path.join(directory, folder, '/**/*.json')))

    let count = 0;

    await asyncForEach(files, async (file) =>{
      const doc = JSON.parse(fs.readFileSync(file, 'utf8')); //load(file)
      const selectedDocInfo = Object.fromEntries(Object.entries(doc).filter(([k,v]) => fields.includes(k)));
      let obj={};
      // assign doc info to object
      obj.name = selectedDocInfo["rdfs:label"].fr[0]
      obj.contact = {
        mail:selectedDocInfo.hasContact[0]['schema:email']?selectedDocInfo.hasContact[0]['schema:email'][0]:null,
        phone:selectedDocInfo.hasContact[0]['schema:telephone']?selectedDocInfo.hasContact[0]['schema:telephone'][0]:null,
        web:selectedDocInfo.hasContact[0]['foaf:homepage']? selectedDocInfo.hasContact[0]['foaf:homepage'][0]:null
      }
      obj.comment = {
        fr : selectedDocInfo["rdfs:comment"] && selectedDocInfo["rdfs:comment"].fr? selectedDocInfo["rdfs:comment"].fr[0] : null,
        en : selectedDocInfo["rdfs:comment"] && selectedDocInfo["rdfs:comment"].en? selectedDocInfo["rdfs:comment"].en[0] : null,
      }
      if(!selectedDocInfo.hasDescription) obj.description = {fr:null,en:null}
      else {
        obj.description = {
          fr : selectedDocInfo.hasDescription[0].shortDescription && selectedDocInfo.hasDescription[0].shortDescription.fr? selectedDocInfo.hasDescription[0].shortDescription.fr[0] : null,
          en : selectedDocInfo.hasDescription[0].shortDescription && selectedDocInfo.hasDescription[0].shortDescription.en? selectedDocInfo.hasDescription[0].shortDescription.en[0] : null
        }
      }
      if(!selectedDocInfo.hasRepresentation) obj.pictures = []
      else {
        obj.pictures = selectedDocInfo.hasRepresentation.map(p => {if (p["ebucore:hasRelatedResource"]) return p["ebucore:hasRelatedResource"][0]["ebucore:locator"][0]})
      }
      const lat = selectedDocInfo.isLocatedAt[0]["schema:geo"]["schema:latitude"]
      const lon = selectedDocInfo.isLocatedAt[0]["schema:geo"]["schema:longitude"]
      obj.geom = `POINT(${lon} ${lat})`

      obj.startDate = selectedDocInfo["schema:startDate"]? selectedDocInfo["schema:startDate"][0] : null
      obj.endDate = selectedDocInfo["schema:endDate"]? selectedDocInfo["schema:endDate"][0] : null
      obj.lastUpdate = selectedDocInfo.lastUpdate || null

      // load object to table
      await pool.query('INSERT INTO import.dt_poi (\
        name, email, phone, web, startdate, enddate, comment, description, pictures, tag, last_update, geom) \
        VALUES ($1, $2,$3, $4,$5,$6,$7,$8,$9,$10,$11,ST_Transform(ST_GeomFromText($12,4326), 3857))', 
        [obj.name, obj.contact.mail, obj.contact.phone, obj.contact.web, obj.startDate, obj.endDate, obj.comment.fr, obj.description.fr, obj.pictures, tag, obj.lastUpdate, obj.geom]
      )     
      
      count += 1
    })

    console.log(count, ' files loaded');
    console.log("DONE import " + tag);
    return "OK";

  } catch (error) { 
    console.log(error);
  }
})();
