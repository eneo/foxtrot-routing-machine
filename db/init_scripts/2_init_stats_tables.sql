CREATE SCHEMA IF NOT EXISTS auth;
CREATE TABLE auth.users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50),
    email VARCHAR(50),
    key VARCHAR(50),
    access_count INT,
    access_count_this_month INT,
    date_created TIMESTAMP,
    date_expire TIMESTAMP
);

CREATE SCHEMA IF NOT EXISTS records;
CREATE TABLE records.routes (
    id UUID PRIMARY KEY,
    cookie VARCHAR(500),
    apikey VARCHAR(50),
    date TIMESTAMP,
    geom_start geometry(Point,4326),
    geom_end geometry (Point,4326),
    route_geom geometry (Linestring, 4326),
    route_duration DECIMAL,
    route_distance DECIMAL,
    pois JSONB,
    profil VARCHAR(50),
    alternative VARCHAR(50),
    url VARCHAR(500)
);