CREATE SCHEMA IF NOT EXISTS import;
CREATE TABLE import.dt_poi (
    id SERIAL PRIMARY KEY,
    name VARCHAR(150),
    email VARCHAR(50),
    phone VARCHAR(50),
    web VARCHAR(150),
    startdate DATE,
    enddate DATE,
    comment TEXT,
    description TEXT,
    pictures VARCHAR(250)[],
    tag VARCHAR(50),
    last_update DATE,
    geom GEOMETRY (Point,3857)
);