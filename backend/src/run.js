const logfmt = require("logfmt");
const app = require("./app");
const path = require('path')

let osrmGraphs = []
Object.keys(process.env).forEach(function(key,index) {
  if (key.startsWith("OSRM_GRAPH_")){
    osrmGraphs.push({osrmDataPath: path.resolve(process.env[key])})
  }
});

const server = app.createServer(osrmGraphs)

server.listen(80, () => {
  logfmt.log({ "start": "running server",
               "address": server.address().address,
               "port": server.address().port
               //"osrm-dataset": process.env.OSRM_GRAPH
             });
});
