const { v4: uuidv4 } = require('uuid');

const recordRequest = async (req, res, next) => {
    // req.headers.cookie client-ip

    const queryParams = new URLSearchParams(req.query);
    const sender = queryParams.get('sender');
    if (sender == "chatbot") {console.log('sender is chatbot'); next();}
    
    const  
      apikey = req.headers['api-key']|| req.query.apikey,
      cookie = req.headers.cookie,
      url = req.url,
      profil = req.query.profil,
      alternative = req.query.alternatives;

    let coords=null;
    if (req.path && req.path.length>1) {
      coords = req.path.split('/')[3].split(';').map(coord => coord.split(',').map(c=> Number(c)));
    } else {
      coords = getCoordinatesFromQuery(queryParams) || req.body.coordinates || null;
    } 
    if (!coords) {
      console.log("Missing coordinates"); 
      next();
    }

    const geom_start = `POINT(${coords[0][0]} ${coords[0][1]})`
    const geom_end = `POINT(${coords[1][0]} ${coords[1][1]})`
    const uuid = uuidv4();

    const pool = req.app.get("pool"); 
    
    try {
      //console.log(uuid, cookie, apikey, geom_start, geom_end, profil, alternative, url)
      await pool.query('INSERT INTO records.routes (\
        id, cookie, apikey, date, geom_start, geom_end, profil, alternative, url) \
        VALUES ($1, $2,$3, NOW(), ST_GeomFromText($4,4326), ST_GeomFromText($5,4326),$6,$7, $8)', 
        [uuid, cookie, apikey, geom_start, geom_end, profil, alternative, url]
      )
      console.log('request recorded')

      res.locals.recordId = uuid;
    } catch (err) {
      console.log( {error: err.message });
      next();
    }
    next();
}

const recordResponse = async (req, res) => {
  const queryParams = new URLSearchParams(req.query);
  const sender = queryParams.get('sender');
  if (sender == "chatbot") {
    console.log('sender is chatbot'); 
    return res.status(200).send(res.locals.result);
  }

  const 
    alternative = req.query.alternatives,
    routeChoosen = alternative === 'true'? 0 : alternative,
    route_geom = JSON.stringify(res.locals.result.routes[routeChoosen].geometry),
    route_duration = res.locals.result.routes[routeChoosen].duration,
    route_distance = res.locals.result.routes[routeChoosen].distance; 
  let pois = '[]'
  if (res.locals.result.routes[routeChoosen].pois) {
    pois = JSON.stringify(res.locals.result.routes[routeChoosen].pois.map(poi => poi.properties.fid));
  }
  const pool = req.app.get("pool"); 
  try {
    await pool.query(`UPDATE records.routes
      SET 
        route_geom = ST_GeomFromGeoJSON('${route_geom}'),
        route_duration = ${route_duration},
        route_distance = ${route_distance},
        pois = '${pois}'
      WHERE id = '${res.locals.recordId}';`   
    )
    console.log('record updated')
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
  return res.status(200).send(res.locals.result);
}

function getCoordinatesFromQuery(queryParams) {
  if (queryParams.has("loc")) {
    var queryCoordinates = [];
    var str = queryParams.getAll('loc')[0];
    var locations = str.split(",");
    for (var i = 0, len = locations.length; i < len; i += 2) {
      var loc = [parseFloat(locations[i]), parseFloat(locations[i + 1])];
      queryCoordinates.push(loc);
    }
    return queryCoordinates;
  }
  else {
    return null;
  }
}

module.exports = {
    recordRequest,
    recordResponse
  }
  