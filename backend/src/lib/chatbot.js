function createChatbotResponse(result, osrmOptions) {

    var locations = [];
    for (var waypoint of result.waypoints) {
        locations.push(waypoint.location);
    }

    var response = { 
        "botnation": "v1",
        "reply": [{
            "type": "text",
            "value": "Itinéraire(s) proposé(s)"
            }, {
            "type": "carousel",
            "ratio": "horizontal",
            "pages": []
        }]
    };
    var carousel = response.reply.find(item => item.type == "carousel");
    
    for (var i = 0, len = result.routes.length; i < len; i++) {
        var route = result.routes[i];
        var start = locations[0];
        var end = locations[locations.length-1];
        var duration = Math.floor(route.duration / 60);
        var distance = (route.distance / 1000).toFixed(1);
        var redirect_url = `${process.env.BACKEND_URL}/v1/route/?loc=${start}&loc=${end}&alt=${i}&poi=${osrmOptions.poi}&profil=${osrmOptions.profil}&apikey=${process.env.CHATBOT_APIKEY}`;
        carousel.pages.push({
            "title": "Trajet " + (i + 1),
            "subtitle": distance + " km - " + duration + " min",
            "image": "https://upload.wikimedia.org/wikipedia/commons/9/9b/Orl%C3%A9ansMetropole.jpg",
            "link": redirect_url,
            "buttons": [{
                "label": "Go !",
                "type": "web_url",
                "url": redirect_url
            }]
        })
    }

    return response;
}

module.exports = {
    createChatbotResponse
};
