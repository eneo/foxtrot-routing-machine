//const crypto = require('crypto');
const { v4: uuidv4 } = require('uuid');
const auth = require("../controllers/authController");
/*
function encryptedSecretKey () {
  return crypto.createHmac('sha256', process.env.APP_SECRET).digest('hex');
}
*/


// Create the collection of api keys
const apiKeys = new Map();
apiKeys.set(uuidv4(), {
  id: 1,
  name: 'app1',
  secret: 'secret1'
});
apiKeys.set(uuidv4(), {
  id: 2,
  name: 'app2',
  secret: 'secret2'
});
console.log('api keys',apiKeys);


function checkAdmin(req,res,next) {
  let clientApiKey = req.get('api-key');
  if(clientApiKey !== process.env.APP_SECRET) {
    return res.status(401).send({
      status:false,
      response:"Permission denied"
    });
  }
  console.log('check admin ok');
  next();
}
// https://medium.com/better-programming/getting-data-from-mongodb-creating-an-api-key-validation-middleware-in-express-944382205d3e

async function clientApiKeyValidation(req,res,next) {
  let clientApiKey = req.get('api-key') || req.query.apikey;
  if(!clientApiKey){
    return res.status(401).send({
      status:false,
      response:"Api key required"
    });
  }
  try {
    let clientDetails = await auth.getUserByKey(req, res, clientApiKey);
    if (!clientDetails || clientDetails.length === 0) {
      console.log('Invalid Api Key');
      return res.status(401).send({
        status: false,
        response: "Invalid Api key"
      });
    }
    await auth.updateUserAccessCount(req, clientDetails[0].email)
    console.log('check client Api key ok');
    next();
  } catch (err) {
    console.log(err)
  }
}

module.exports = {
  checkAdmin,
  clientApiKeyValidation
};
  