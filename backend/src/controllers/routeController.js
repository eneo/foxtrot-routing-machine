const decode = require('geojson-polyline').decode
const qr = require("./poisController");
const chatbot = require("../lib/chatbot");
const logfmt = require("logfmt");


const frontend_route = async (req, res, next) => {
  //console.log('frontendroute',req.params)
  const coords = req.params.coords.split(';').map(coord => coord.split(',').map(c=> Number(c)));
  if (!coords) {
    return res.status(422).json({ error: "Missing coordinates" });
  }
  
  const queryParams = new URLSearchParams(req.query);
  //console.log('queryParams', queryParams);
  
  const options = {
    coordinates: coords,
    alternatives: queryParams.get('alternatives') === 'true', 
    //|| queryParams.get('alternatives') === 'false'? 
    //  queryParams.get('alternatives') : parseInt(queryParams.get('alternatives')),    
    // Return route steps for each route leg
    steps: queryParams.get('steps')=='true' ,
    // Return annotations for each route leg
    annotations: queryParams.get('annotations')=='true' ,
    // Returned route geometry format. Can also be geojson
    geometries: queryParams.get('geometries') || "polyline",
    // Add pois along choosen route
    // Boolean
    poi: queryParams.get('poi')=='true',
    // Set OSRM profil
    profil: queryParams.get('profil') || 'osrm-green-foot'
  };
  const osrm = req.app.get(options.profil);
  try {
    osrm.route(options, async (err, result) => {
      //console.log('options', options);
      //console.log('routes',result.routes);
      if (err) {
        return res.status(422).json({ error: err.message });
      }
      let routes =[];
      if (options.geometries === 'polyline') {
        routes = result.routes.map(route =>
          ({...route, geometry: decode({
            type: 'LineString',
            coordinates: route.geometry
          })
        }));
      } else {
        routes = result.routes;
      } 
      result.code = "Ok";
      if (options.poi === true) {
        try {
          await asyncForEach(routes, async (route) => {
            //console.log('async route', route.geometry);
            route.pois = await qr.getPois(req, res, JSON.stringify(route.geometry), options.profil);
            
            route.pois = route.pois.map(poi => 
              ({...poi, properties: 
                Object.entries(poi.properties).reduce((a,[k,v]) => (
                v == null ? a : {...a, [k]:v}
                ), {})
              })
            ); 
          });
          
        } catch (err) {
          return res.status(500).json({ errorforeach: err.message });
        }
        
      }
      result.routes = routes;
      res.locals.result = result
      next() 
      
    });
  } catch (err) {
    logfmt.error(new Error(err.message));
    return res.status(500).json({ error: err.message });
  }
};

const main_route = async (req, res, next) => {
  console.log('mainroute')

  const queryParams = new URLSearchParams(req.query);
  const coordinates = getCoordinatesFromQuery(queryParams) || req.body.coordinates || null;
  if (!coordinates) {
    return res.status(422).json({ error: "Missing coordinates" });
  }
  
  //check if alternative chosen (from the chatbot for example) -> redirect to the frontend
  const alternative = queryParams.get('alt');
  if (alternative != null)
  {
    const start = inverseCoordinates(coordinates[0]),
     end = inverseCoordinates(coordinates[coordinates.length-1]),
     poi = queryParams.get('poi'),
     profil = queryParams.get('profil') || 'osrm-green-foot',
     apikey = req.headers['api-key']|| req.query.apikey;
    res.writeHead(301,
      {Location: `${process.env.BACKEND_URL}/map/?loc=${start}&loc=${end}&alt=${alternative}&poi=${poi}&profil=${profil}&apikey=${apikey}`}
    );
    res.end();
    return res;
  }

  //otherwise compute routes with osrm
  const osrmOptions = {
    coordinates: coordinates,
    alternatives: queryParams.get('alternatives') === 'true' || queryParams.get('alternatives') === 'false'? 
      queryParams.get('alternatives') : parseInt(queryParams.get('alternatives')),    
    // Return route steps for each route leg
    steps: req.body.steps || true,
    // Return annotations for each route leg
    annotations: req.body.annotations || false,
    // Returned route geometry format. Can also be geojson
    geometries: req.body.geometries || "geojson",
    // Add overview geometry either full, simplified according to
    // highest zoom level it could be display on, or not at all
    overview: req.body.overview || "false",
    // Forces the route to keep going straight at waypoints and don't do
    // a uturn even if it would be faster. Default value depends on the profile
    continue_straight: req.body.continue_straight || false,
    // Add pois along choosen route
    // Boolean
    poi: queryParams.get('poi')=='true',
    // Set OSRM profil
    profil: queryParams.get('profil') || 'osrm-green-foot'
  };
  const osrm = req.app.get(osrmOptions.profil);
  
  try {
    osrm.route(osrmOptions, async (err, result) => {
      //console.log('options',osrmOptions);
      //console.log('routes',result.routes);
      if (err) {
        return res.status(422).json({ error: err.message });
      }
      var sender = queryParams.get('sender');
      if (sender == "chatbot") {
        return res.json(chatbot.createChatbotResponse(result, osrmOptions));
      }
      else {
        let routes =[];
        if (osrmOptions.geometries === 'polyline') {
          routes = result.routes.map(route =>
            ({...route, geometry: decode({
              type: 'LineString',
              coordinates: route.geometry
            })
          }));
        } else {
          routes = result.routes;
        } 
        if (osrmOptions.poi === true) {
          try {
            await asyncForEach(routes, async (route) => {
              route.geometry = {
                type: 'LineString',
                coordinates: route.legs.map(l=> 
                  [].concat.apply([],l.steps.map(s=> 
                    [].concat.apply([],s.geometry.coordinates
                    )
                  ))
                )   
              };         
              route.pois = await qr.getPois(req, res, JSON.stringify(route.geometry));
              route.pois = route.pois.map(poi => 
                ({...poi, properties: 
                  Object.entries(poi.properties).reduce((a,[k,v]) => (
                  v == null ? a : {...a, [k]:v}
                  ), {})
                })
              ); 
            });
            
          } catch (err) {
            return res.status(500).json({ errorforeach: err.message });
          }        
        }
        result.routes = routes;
        res.locals.result = result
        next()
      }
    });
  } catch (err) {
    logfmt.error(new Error(err.message));
    return res.status(500).json({ error: err.message });
  }
};

function getCoordinatesFromQuery(queryParams) {
  if (queryParams.has("loc")) {
    var queryCoordinates = [];
    var str = queryParams.getAll('loc')[0];
    var locations = str.split(",");
    for (var i = 0, len = locations.length; i < len; i += 2) {
      var loc = [parseFloat(locations[i]), parseFloat(locations[i + 1])];
      queryCoordinates.push(loc);
    }
    return queryCoordinates;
  }
  else {
    return null;
  }
}

function inverseCoordinates(coords){
  if (coords.length == 2){
    var newCoords = [];
    newCoords.push(coords[1], coords[0]);
    return newCoords;
  }
  else {
    return coords;
  }
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

//async function addPoisToRoute(req, res, route) {  
//  route.pois = await qr.getPois(req, res, JSON.stringify(route.geometry));
//  route.pois = route.pois.map(poi => 
//    ({...poi, properties: 
//      Object.entries(poi.properties).reduce((a,[k,v]) => (
//      v == null ? a : {...a, [k]:v}
//      ), {})
//    })
//  ); 
//  return route; 
//}

module.exports = {
  frontend_route,
  main_route
}