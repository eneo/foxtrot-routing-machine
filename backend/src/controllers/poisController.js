

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

const sqlRequest = (mapping, route) => {
  return `SELECT *, st_asgeojson(ST_Transform(geom,4326)) as geom 
  FROM import.${mapping.ref_table}
  WHERE (
    tag IN ('${mapping.tag}') 
  AND ST_Intersects(geom, ST_Buffer( 
      ST_Transform( 
        ST_GeomFromGeoJSON('${route}') 
        , 3857)
      , ${mapping.distance}))
  )`
}

const getPois = async (req, res, route, profil) => {
  console.log('in getPois');
  const pool = req.app.get("pool");
  const profilTheme = profil.split('-')[1]
  const poiFiles = req.app.get("poisFiles")
  let poiMapping = poiFiles.filter(config => config.profiles.includes(profilTheme))
  let queryResults = {rows:[]}
  try {
    await asyncForEach(poiMapping, async(mapping) => {
      const queryPoints = sqlRequest(mapping, route)
      const queryPointsResults = await pool.query(queryPoints); 
      queryResults.rows.push(queryPointsResults.rows)
    })
    queryResults.rows = [].concat.apply([],queryResults.rows)
    let poiConfigByName = {}
    console.log('query rows length:',queryResults.rows.length)
    poiMapping.forEach(c => {
      if (c.displayLimit) {
        // filter POI if more than limit per tag
        queryResults.rows = queryResults.rows.filter(row => row.tag !== c.tag).concat(
          ...poiFilter(queryResults.rows.filter(row => row.tag === c.tag),c.displayLimit)
        )
      }
      
      poiConfigByName[c.tag] = c
    });
    // make proper geojson out of sql query result
    queryResults.rows = queryResults.rows.map(row => { 
      let properties = []
      // if POI has properties to be shown in popup, add properties
      if (poiConfigByName[row.tag].clickable) {
        properties = 
          poiConfigByName[row.tag].properties.map(p => {
            p.value = row[p.column]
            return p
          })
      }
      poiConfigByName[row.tag].properties = properties
      const poiProperties = JSON.parse(JSON.stringify(poiConfigByName[row.tag]))
      delete poiProperties.ref_table
      delete poiProperties.profiles
      
      return {
        "geometry":JSON.parse(row.geom), 
        "type":'Feature', 
        "properties": poiProperties
      }
    });
    const tagList = setDocListByIndexes(queryResults.rows);
    console.log(tagList);
    return queryResults.rows    
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
};

// poi filter
// only profil tags
// not genericTags
//** favor poi with main picture link
// maximum 3 pois with same tag in a route
function poiFilter(pois, displayLimit) {
  console.log('filter pois',pois[0].tag)
  if (pois.length > displayLimit) {
    pois = randomSubset(pois, displayLimit)
  }
  return pois;
}


/////////
// Tools
/////////

function setDocListByIndexes (documents) {     
  console.log('in setdoc')
  let tagsList = [];
  documents.forEach(doc => {
    if (tagsList.length === 0 || !getUniqueValuesOfKey(tagsList, 'tag').includes(doc.properties.tag)) {
      tagsList.push({'tag': doc.properties.tag, 'count':1});  
    } else {
      tagsList.map(el => {if (el.tag === doc.properties.tag) {el.count +=1 ;}});  
    }
  });
  return tagsList;
}

function getUniqueValuesOfKey (array, key) {
  return array.reduce(function(list, item) {
    if(item[key] && list.indexOf(item[key]) ===-1) {
      list.push(item[key]);
    }
    return list;
  }, []);
}

function randomSubset(arr, size) {
  var shuffled = arr.slice(0), i = arr.length, temp, index;
  while (i--) {
      index = Math.floor((i + 1) * Math.random());
      temp = shuffled[index];
      shuffled[index] = shuffled[i];
      shuffled[i] = temp;
  }
  return shuffled.slice(0, size);
}

module.exports = {
  getPois
}
