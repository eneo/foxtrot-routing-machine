const { v4: uuidv4 } = require('uuid');

const getUsers = async(req, res) => {
  const pool = req.app.get("pool");
  try {
    const result = await pool.query('SELECT * FROM auth.users ORDER BY id ASC');
    return res.status(200).json(result.rows)
  } catch (err) {
    console.log('get users query err',err)
  }
}

const getUserByKey = async (req, res, key) => {
  const pool = req.app.get("pool");
  try {
    const result = await pool.query('SELECT * FROM auth.users WHERE key = $1', [key]);
    return result.rows
  } catch (err) {
    console.log('user key query err',err)
  }
}

const getUserByEmail = async (req, email) => {
  const pool = req.app.get("pool");
  try {
    const result = await pool.query('SELECT * FROM auth.users WHERE email = $1', [email]);
    return result.rows
  } catch (err) {
    console.log('email query err',err)
  }
}

const createUser = async (req, res) => {
  const pool = req.app.get("pool");
  console.log('body', req.body)
  const { name, email } = req.body;
  if (!email) return res.status(400).json({ auth : false, message :"no email provided"});
  try {
    console.log('checking email')
    const user = await getUserByEmail(req, email);
    if(user.length !== 0) return res.status(400).json({ auth : false, message :"email already exists", user: user});
  
    const key = uuidv4();
    const date_created = Math.floor(Date.now()/1000);
    await pool.query('INSERT INTO auth.users (name, email, key, date_created, access_count, access_count_this_month) VALUES ($1, $2,$3, NOW(), 0, 0)', [name, email, key])
    
    return res.status(201).send('User successfully added to database')
  } catch(err) {
    console.log('email err',err)
  }
  
}

const updateUser = async (req, res) => {
  const pool = req.app.get("pool");
  const email = req.body.email
  if(!email) return res.status(400).json({ auth : false, message :"no email provided"});
  try {
    console.log('checking email')
    const user = await getUserByEmail(req, email);
    if(user.length === 0) return res.status(400).json({ auth : false, message :"no user with this email"});
    
    let sqlquery = 'UPDATE auth.users SET ';
    Object.entries(req.body).forEach(([k,v]) => {
      if (k === 'email') return
      sqlquery += `${k} = '${v}', `;
    })
    
    if (sqlquery.slice(-2)===', ') sqlquery = sqlquery.slice(0, -2)
    sqlquery += ` WHERE email = '${email}';`
    console.log('update query',sqlquery)
  
    const result = await pool.query(sqlquery)
    return res.status(200).send('User successfully updated')
  } catch(err) {
    console.log('update err',err)
  }
}

const updateUserAccessCount = async (req, email) => {
  const pool = req.app.get("pool");
  try {
    console.log('update count for',email)
    const result = await pool.query('Update auth.users SET access_count = access_count + 1 WHERE email = $1',[email])
    console.log('result',result)
  } catch(err) {
    console.log('update access count err',err)
  }
}
      
const deleteUserByEmail = async (req, res) => {
  const pool = req.app.get("pool");
  const email = req.body.email
  try {
    await pool.query('DELETE FROM auth.users WHERE email = $1', [email])
    
    return res.status(200).send('User successfully deleted')
  } catch(err) {
    console.log('delete err',err)
  }
  
}
const deleteUserById = async (req, res) => {
  const pool = req.app.get("pool");
  const id = req.body.id
  try {
    await pool.query('DELETE FROM auth.users WHERE id = $1', [id])
    
    return res.status(200).send('User successfully deleted')
  } catch(err) {
    console.log('delete err',err)
  }
  
}

module.exports = {
  getUsers,
  getUserByKey,
  getUserByEmail,
  createUser,
  updateUser,
  updateUserAccessCount,
  deleteUserByEmail,
  deleteUserById
}
// source: https://medium.com/@sarthakmittal1461/to-build-login-sign-up-and-logout-restful-apis-with-node-js-using-jwt-authentication-f3d7287acca2

/*
const bcrypt=require('bcrypt');
const jwt=require('jsonwebtoken');
// sign-up route
app.post('/api/register',function(req,res){
  // taking a user
  const newuser=new User(req.body);
  
  //if(newuser.password!=newuser.password2)return res.status(400).json({message: "password not match"});
  
  User.findOne({email:newuser.email},function(err,user){
      if(user) return res.status(400).json({ auth : false, message :"email already exists"});

      newuser.token = uuidv4();
      newuser.save((err,doc)=>{
          if(err) {console.log(err);
              return res.status(400).json({ success : false});}
          res.status(200).json({
              succes:true,
              user : doc
          });
      });
  });
});
// login user
app.post('/api/login', function(req,res){
  let token=req.cookies.auth;
  User.findByToken(token,(err,user)=>{
    if(err) return  res(err);
    if(user) return res.status(400).json({
      error :true,
      message:"You are already logged in"
    });
    
    else {
      User.findOne({'email':req.body.email},function(err,user) {
        if(!user) return res.json({isAuth : false, message : ' Auth failed ,email not found'});

        user.comparepassword(req.body.password,(err,isMatch)=>{
          if(!isMatch) return res.json({ isAuth : false, message : "password doesn't match"});
        });
        user.generateToken((err,user)=>{
          if(err) return res.status(400).send(err);
          res.cookie('auth',user.token).json({
            isAuth : true,
            id : user._id,
            email : user.email
          });
        });    
      });
    }
  });
});
//logout user
app.get('/api/logout',auth,function(req,res){
  req.user.deleteToken(req.token,(err,user)=>{
      if(err) return res.status(400).send(err);
      res.sendStatus(200);
  });
}); 

// hash password pre-save
userSchema.pre('save',function(next){
    var user=this;
    
    if(user.isModified('password')){
        bcrypt.genSalt(salt,function(err,salt){
            if(err)return next(err);

            bcrypt.hash(user.password,salt,function(err,hash){
                if(err) return next(err);
                user.password=hash;
                next();
            })

        })
    }
    else{
        next();
    }
});
// compare password
userSchema.methods.comparepassword=function(password,cb){
  bcrypt.compare(password,this.password,function(err,isMatch){
    if(err) return cb(next);
    cb(null,isMatch);
  });
}
// generate token
userSchema.methods.generateToken=function(cb){
  var user =this;
  var token=jwt.sign(user._id.toHexString(),confiq.SECRET);

  user.token=token;
  user.save(function(err,user){
      if(err) return cb(err);
      cb(null,user);
  })
}
// find by token
userSchema.statics.findByToken=function(token,cb){
  var user=this;

  jwt.verify(token,confiq.SECRET,function(err,decode){
      user.findOne({"_id": decode, "token":token},function(err,user){
          if(err) return cb(err);
          cb(null,user);
      })
  })
};
// delete token
userSchema.methods.deleteToken=function(token,cb){
  var user=this;

  user.update({$unset : {token :1}},function(err,user){
      if(err) return cb(err);
      cb(null,user);
  })
}

*/