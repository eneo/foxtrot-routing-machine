

const getRecords = async (req, res) => {
  console.log('in getRecords');
  const pool = req.app.get("pool");
  var queryPoints = `SELECT *,\
  st_asgeojson(ST_Transform(geom_start,4326)) as geom_start, \
  st_asgeojson(ST_Transform(geom_end,4326)) as geom_end, \
  st_asgeojson(ST_Transform(route_geom,4326)) as route_geom \
  FROM records.routes;`;

  try {
    const queryResults = await pool.query(queryPoints);
    return res.status(200).send(queryResults.rows);
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
};

module.exports = {
  getRecords
}
