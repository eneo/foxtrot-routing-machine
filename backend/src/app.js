const http = require("http");
const express = require("express");
const cors = require('cors'); //https://expressjs.com/en/resources/middleware/cors.html
const bodyParser = require("body-parser");
const helmet = require("helmet");
const logfmt = require("logfmt");

const health = require("./routes/health");
const route = require("./routes/route");
const table = require("./routes/table");
const pg = require("./routes/pg");
const map = require("./routes/map");
const record = require("./routes/record");
const auth = require("./routes/auth");

const osrmBindings = require("./lib/osrm");
const pgBindings = require("./lib/pgconnexion");
const apiKey = require('./lib/crypto');
const recordMiddleware = require('./lib/recordMiddleware');

const {getOsPath} = require('./config/utils');
const fs = require('fs');
function configureMiddlewares(app) {
  // overall api-key required
  //app.use(apiKey.clientApiKeyValidation);
  app.use(helmet());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(logfmt.requestLogger());
  if (process.env.NODE_ENV === 'dev') {
    app.use(cors());
  }
  
}

function configureRoutes(app) {
  app.use("/health", health);
  //app.use("/route", route);
  app.use("/v1/route", apiKey.clientApiKeyValidation, recordMiddleware.recordRequest, route, recordMiddleware.recordResponse);
  app.use("/table", table);
  app.use("/pg", pg);
  app.use('/map', map);
  app.use('/records', apiKey.checkAdmin,record);
  app.use('/auth', apiKey.checkAdmin, auth);
}

function configureOSRM(app, options) {
  options.forEach(opt => {
    console.log('configure OSRM',opt);    
      app.set(
        opt.osrmDataPath.split('/')
        .find(p=>p.includes('osrm-')), 
        osrmBindings.loadGraph(opt)
      );
    }
  );
}

function configurePG(app) {
  const pool = pgBindings.pgConnect();
  app.set("pool", pool);
}

function configurePois(app) {
  const poisDir = getOsPath('POI_CONFIG_FILES_DIR')
  const filesPath = fs.readdirSync(poisDir);
  let files = [];
  filesPath.forEach(file => {
    if (file.split('.').slice(-1)[0] !== 'json') return
    let config = JSON.parse(fs.readFileSync(poisDir+file , 'utf8'))
    //console.log('poisConfig length', config.length)
    files = [...files, config]
  })
  //console.log("files",files)
  app.set("poisFilesPath", filesPath);
  app.set("poisFiles", [].concat.apply([],files));
  app.set("poisDir", poisDir);
}

function createServer(options) {
  const opts = options || [];
  const app = express();
  configureOSRM(app, opts);
  configurePG(app);
  configureMiddlewares(app);
  configureRoutes(app);
  configurePois(app);
  return http.createServer(app);
}

module.exports = {
  createServer
};
