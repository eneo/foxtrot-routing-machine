const express = require("express");
//const logfmt = require("logfmt");

var route_controller = require('../controllers/routeController');

const router = express.Router();

router.get("/", route_controller.main_route);

router.get("/v1/driving/:coords", route_controller.frontend_route);

module.exports = router;