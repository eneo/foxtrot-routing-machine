const { createProxyMiddleware } = require('http-proxy-middleware');

const options = {
target: process.env.FRONTEND_URL,
changeOrigin: true,
ws: true, 
pathRewrite: {'^/map' : ''}
};
const frontendProxy = createProxyMiddleware(options);

module.exports = frontendProxy;