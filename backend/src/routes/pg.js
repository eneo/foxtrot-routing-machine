const express = require("express");
const pois_controller = require('../controllers/poisController');
const router = express.Router();

router.get("/", (req, res, next) => {
  res.status(200).json({ API: "Postgres API ok" });
});

router.get("/pois", pois_controller.getPois);

module.exports = router;
