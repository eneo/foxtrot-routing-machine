const express = require("express");
const record_controller = require('../controllers/recordController');
const router = express.Router();

router.get("/", (req, res, next) => {
  res.status(200).json({ API: "recordsok" });
});

router.get("/all", record_controller.getRecords);

module.exports = router;
