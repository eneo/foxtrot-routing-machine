const express = require("express");

var auth_controller = require('../controllers/authController');

const router = express.Router();

router.get('/', auth_controller.getUsers)
//router.get('/getUserByKey', auth_controller.getUserByKey)
//router.get('/getUserByEmail', apiKey.checkAdmin, auth_controller.getUserByEmail)
router.post('/', auth_controller.createUser)
router.put('/', auth_controller.updateUser)
router.delete('/due', auth_controller.deleteUserByEmail)
router.delete('/dui', auth_controller.deleteUserById)

module.exports = router;