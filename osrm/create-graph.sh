#!/bin/bash

#on crée un fichier pour stocker les valeurs exactes des rates
#printf "osm_id, rate\n" > /opt/final_rates.csv

set -e

osrm-extract -p $PROFILE $PBF
osrm-partition $GRAPH
osrm-customize $GRAPH

exec "$@"
