#!/bin/bash

#on crée un fichier pour stocker les valeurs exactes des rates
#printf "osm_id, rate\n" > /opt/final_rates.csv

set -e

osrm-partition $OSRM_GRAPH
osrm-customize $OSRM_GRAPH
osrm-routed --algorithm mld $OSRM_GRAPH

exec "$@"
