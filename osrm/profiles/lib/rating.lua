
local postgres = require('luasql.postgres').postgres()
sql_conn = assert(postgres:connect('postgresql://'.. os.getenv("POSTGRES_USER") ..':'.. os.getenv("POSTGRES_PASSWORD") .. '@' .. os.getenv("POSTGRES_HOST") .. '/' .. os.getenv("POSTGRES_DB")))

function getCoeffsTags(coeffsTable)
  local n=0
  local keyset={}

  for k,v in pairs(coeffsTable) do
    n=n+1
    keyset[n]="'" .. k  .. "'"
  end
  tags = table.concat(keyset, ",")
  return(tags)
end



function getLinestring(way)
  local linestring = {}
  local nodes = way:get_nodes()
  local n = 0

  for _, node in pairs(nodes) do

    if node:location():valid() then
      table.insert(linestring, node:location():lon() .. " " ..node:location():lat())
      n = n + 1
    else
      assert(not("No valid location on NodeRef"))
    end
  end

  if n <= 1 then
    return {0, 0, 0, 0}
  end

  linestring = table.concat(linestring, ",")

  return linestring
end



function setRatesByNoise(way, result)
  query_bruit = [[
SELECT id,val, ST_LENGTH(
  ST_Intersection(
    geom, 
    ST_Transform(
      ST_GeomFromText(
	    'LINESTRING(%s)' ,
		4326
	  ),
	  3857
	)
  )
) as length
FROM import.odom_bruit
WHERE ST_Intersects(
  geom, 
  ST_Transform(
    ST_GeomFromText(
     'LINESTRING(%s)' ,
     4326
	),
	3857
  )
);
]]

  linestring = getLinestring(way)

  local sql = string.format(query_bruit,linestring,linestring)
  local cur = assert(sql_conn:execute(sql))
  row = cur:fetch ({}, "a")  


  local longueurTot = 0
  local bruitCumul = 0
  local bruitMoyen = 0
  while row do
    longueurTot = longueurTot + row["length"]
    bruitCumul = bruitCumul + row["length"] * row["val"]
    row = cur:fetch ({}, "a")
  end

  if longueurTot ~= 0 then
    bruitMoyen = bruitCumul / longueurTot
    -- Ici on va faire passer de l'intervalle 50 --> 70 à 0 --> 100
    bruitMoyen = (bruitMoyen -50) * 100/(70-50)    
  end

  coeff = (100 - bruitMoyen)

  result.forward_rate = result.forward_rate + coeff
  result.backward_rate = result.backward_rate + coeff



end

function setRatesByAOIThrough(way, result, coeffsThroughAOI)
  queryThroughAOI = [[
SELECT osm_id, tag 
  FROM import.osm_aoi
  WHERE ST_Intersects(geom, ST_Buffer(
    ST_Transform(
      ST_GeomFromText('LINESTRING(%s)' ,4326)
      , 3857)
    , 0.1))
  AND tag IN (%s)
;
]]

  linestring = getLinestring(way)

  AOIThroughTags = getCoeffsTags(coeffsThroughAOI)
  local sql = queryThroughAOI:format(linestring,AOIThroughTags)
  local cur = assert(sql_conn:execute(sql))
  row = cur:fetch ({}, "a")  


  while row do
    coeff = coeffsThroughAOI[row["tag"]]
    if coeff ~= nil then
      result.forward_rate = result.forward_rate + coeff
      result.backward_rate = result.backward_rate + coeff
    end

    row = cur:fetch ({}, "a")  
  end
end

function setRatesByAOIAround(way, result, coeffsAroundAOI)
  queryAroundAOI = [[
SELECT osm_id, tag 
  FROM import.osm_aoi
  WHERE ST_Intersects(geom, ST_Buffer(
    ST_Transform(
      ST_GeomFromText('LINESTRING(%s)' ,4326)
      , 3857)
    , 60))
  AND tag IN (%s)
;
]]

  linestring = getLinestring(way)
  
  local nApparationsAOI = {}
  for k,v in pairs(coeffsAroundAOI) do
    nApparationsAOI[k] = 0
  end
  
  AOIAroundTags = getCoeffsTags(coeffsAroundAOI)
  local sql = queryAroundAOI:format(linestring,AOIAroundTags)
  local cur = assert(sql_conn:execute(sql))
  row = cur:fetch ({}, "a")  
  
  while row do
    coeff = coeffsAroundAOI[row["tag"]]
    if coeff ~= nil then
      nApparationsAOI[row["tag"]] = nApparationsAOI[row["tag"]] + 1
    end
    row = cur:fetch ({}, "a")  
  end

  for tag, n in pairs(nApparationsAOI) do
    coeff = coeffsAroundAOI[tag]
    result.forward_rate = result.forward_rate + coeff
    result.backward_rate = result.backward_rate + coeff
  end
end


function setRatesByPOI(way, result, coeffsPOI, tablePOI, buffer)  
  queryPOI = [[
SELECT tag 
  FROM %s
  WHERE ST_Intersects(geom, ST_Buffer(
    ST_Transform(
      ST_GeomFromText('LINESTRING(%s)' ,4326)
      , 3857)
    , %s))
;
]]

  linestring = getLinestring(way)
  
  local nApparationsPois = {}
  for k,v in pairs(coeffsPOI) do
    nApparationsPois[k] = 0
  end
  
  local sql = queryPOI:format(tablePOI, linestring, buffer)
  local cur = assert(sql_conn:execute(sql))
  row = cur:fetch ({}, "a")  

  -- on prend la racine du nombre de point pour diminuer les écarts de valeurs trop importants entre les rues avec beaucoup de pois de celles avec peu de poi
  while row do
    coeff = coeffsPOI[row["tag"]]
    if coeff ~= nil then
      nApparationsPois[row["tag"]] = nApparationsPois[row["tag"]] + 1
    end
    row = cur:fetch ({}, "a")  
  end

  for tag, n in pairs(nApparationsPois) do
    coeff = coeffsPOI[tag]
    result.forward_rate = result.forward_rate + coeff * math.sqrt(n)
    result.backward_rate = result.backward_rate + coeff * math.sqrt(n)
  end

end



function setRatesByHighway(way, result, coeffName, penaltyName) 
  local highway = way:get_value_by_key("highway")
  coeff = coeffName[highway]
  penalty = penaltyName[highway]
  if coeff ~= nil then
    result.forward_rate = result.forward_rate + coeff
    result.backward_rate = result.backward_rate + coeff
  end  
  if penalty ~= nil then
    result.forward_rate = result.forward_rate ^ penalty
    result.backward_rate = result.backward_rate ^ penalty
  end  
  
end







function saveRates(way, result)
  local highway = way:get_value_by_key("highway")
  if highway ~= nil then
    out = io.open('final_rates.csv','a')
    out:write(way:id() .. ", ")
    out:write(result.forward_rate .. "\n")
    io.close(out)
  end
end

function showElapsedCalculation(way)
  local id = way:id()
  -- j'ai regardé l'ordre de traitement des ways de mes anciens test
  if id == 35377669 then
    print("10%")
  elseif id == 156958414 then
    print("20%")
  elseif id == 210385670 then
    print("30%")
  elseif id == 240382598 then
    print("40%")
  elseif id == 422382319 then
    print("50%")
  elseif id == 705383073 then
    print("60%")
  elseif id == 622702670 then
    print("70%")
  elseif id == 626689457 then
    print("80%")
  elseif id == 751250523 then
    print("90%")
  end
  
    
end



