POITable = 'import.odom_poi'
POIBuffer = 50

coeffsPOI = {
  --osm
  ['tree']                          = 5,
  ['bench']                         = 5,
  ['fountain']                      = 3,

  --odom
  ['Amphibien']                     = 3,
  ['Mammifère']                     = 3,
  ['Insecte']                       = 3,
  ['Oiseau']                        = 3,
  ['Reptile']                       = 3,
  ['arbre']                         = 3,
  ['flore']                         = 10,
  ['Colonne végétale']              = 10,  
  ['Parcs et jardins']              = 3,
  ['Relais Sciences et Nature']     = 50,
  ['Sentiers nature']               = 50,
  ['Zones protégées']               = 3,
}

coeffsAroundAOI = {
  --water
  ['lake']                          = 100,
  ['river']                         = 100,
  ['pond']                          = 100,
  ['oxbow']                         = 100,
  ['lagoon']                        = 100,
  ['reservoir']                     = 100,
  ['canal']                         = 100,
  ['lock']                          = 100,
  ['fish_pass']                     = 100,
  ['reflecting_pool']               = 100,
  ['moat']                          = 100,
  ['wastewater']                   = -1000,
  
  --other
  ['scrub']                         = 25,
  ['wood']                          = 50,
  ['forest']                        = 50,
  ['playground']                    = 25,
}

-- la différence entre wood et forest n'est pas toujours interprétée de la meme manière selon les gens qui remplissent la carte
-- surtout quand ils ne font pas toujours la différence entre une zone boisée pénétrable et impénétrable
-- voir https://wiki.openstreetmap.org/wiki/Forest

coeffsThroughAOI = {
  ['park']                          = 200,
  ['grass']                         = 50,
}

penaltyHighway = {
  ['primary']                       = 1/4, --racine 4ème 
  ['primary_link']                  = 1/4, --racine 4ème
  ['secondary']                     = 1/2, --racine carrée 
  ['secondary_link']                = 1/2, --racine carrée 
}


coeffsHighway = {
  ['primary']                       = 0, 
  ['primary_link']                  = 0, 
  ['secondary']                     = 0, 
  ['secondary_link']                = 0, 
  ['tertiary']                      = 20,
  ['tertiary_link']                 = 20,
  ['road']                          = 30,
  ['service']                       = 50,
  ['track']                         = 50,
  ['steps']                         = 35,
  ['unclassified']                  = 100,
  ['path']                          = 100,
  ['pedestrian']                    = 100,
  ['footway']                       = 100,
  ['residential']                   = 100,
  ['living_street']                 = 100,
 
}

coeffs = {
  coeffsPOI, 
  coeffsAroundAOI, 
  coeffsThroughAOI, 
  coeffsHighway, 
  penaltyHighway,
  POITable,
  POIBuffer
}

return coeffs