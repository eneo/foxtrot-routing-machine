POITable = 'import.odom_poi'
POIBuffer = 200

coeffsPOI = {
  --osm
  ['fountain']                      = 0,
  ['viewpoint']                     = 0,
  ['information']                   = 0,
  ['attraction']                    = 0,
  ['artwork']                       = 0,
  ['building']                      = 0,
  ['church']                        = 0,
  ['memorial']                      = 0,
  ['monument']                      = 0,

  --odom
  ['farm']                          = 200,
  ['market']                        = 200,
}

coeffsAroundAOI = {
  --water
  ['lake']                          = 40,
  ['river']                         = 40,
  ['pond']                          = 40,
  ['oxbow']                         = 40,
  ['lagoon']                        = 40,
  ['reservoir']                     = 40,
  ['canal']                         = 40,
  ['lock']                          = 40,
  ['fish_pass']                     = 40,
  ['reflecting_pool']               = 40,
  ['moat']                          = 40,
  ['wastewater']                    = -1000,
  
  --other
  ['scrub']                         = 25,
  ['wood']                          = 50,
  ['forest']                        = 50,
  ['playground']                    = 100,
  ['farmland']                      = 100,
  ['farmyard']                      = 100,
  ['meadow']                        = 100,
  ['greenfield']                    = 100,
  ['orchard']                       = 100,
  ['vineyard']                      = 100,
  ['greenhouse_horticulture']       = 100,
}

-- la différence entre wood et forest n'est pas toujours interprétée de la meme manière selon les gens qui remplissent la carte
-- surtout quand ils ne font pas toujours la différence entre une zone boisée pénétrable et impénétrable
-- voir https://wiki.openstreetmap.org/wiki/Forest

coeffsThroughAOI = {
  ['park']                          = 10,
  ['grass']                         = 10,
}

penaltyHighway = {
  ['primary']                       = 1/4, --racine 4ème 
  ['primary_link']                  = 1/4, --racine 4ème
  ['secondary']                     = 1/2, --racine carrée 
  ['secondary_link']                = 1/2, --racine carrée 
}


coeffsHighway = {
  ['primary']                       = 0, 
  ['primary_link']                  = 0, 
  ['secondary']                     = 0, 
  ['secondary_link']                = 0, 
  ['tertiary']                      = 10,
  ['tertiary_link']                 = 10,
  ['road']                          = 20,
  ['service']                       = 30,
  ['track']                         = 30,
  ['steps']                         = 0,
  ['unclassified']                  = 30,
  ['path']                          = 30,
  ['pedestrian']                    = 30,
  ['footway']                       = 30,
  ['residential']                   = 30,
  ['living_street']                 = 30,
 
}


coeffs = {
  coeffsPOI, 
  coeffsAroundAOI, 
  coeffsThroughAOI, 
  coeffsHighway, 
  penaltyHighway,
  POITable,
  POIBuffer
}


return coeffs