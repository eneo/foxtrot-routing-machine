POITable = 'import.dt_poi'
POIBuffer = 200

coeffsPOI = {
  -- osm 
  ['tree']                          = 5,
  ['bench']                         = 5,
  ['fountain']                      = 100,
  ['viewpoint']                     = 200,
  ['information']                   = 200,
  ['attraction']                    = 200,
  ['artwork']                       = 200,
  ['building']                      = 200,
  ['church']                        = 200,
  ['memorial']                      = 200,
  ['monument']                      = 200,

  --datatourisme
  ['site']                          = 200,
  ['evenement']                     = 200,
}

coeffsAroundAOI = {
  --water
  ['lake']                          = 50,
  ['river']                         = 50,
  ['pond']                          = 50,
  ['oxbow']                         = 50,
  ['lagoon']                        = 50,
  ['reservoir']                     = 50,
  ['canal']                         = 50,
  ['lock']                          = 50,
  ['fish_pass']                     = 50,
  ['reflecting_pool']               = 50,
  ['moat']                          = 50,
  ['wastewater']                   = -1000,
  
  --other
  ['scrub']                         = 25,
  ['wood']                          = 50,
  ['forest']                        = 100,
  ['playground']                    = 25,
}

-- la différence entre wood et forest n'est pas toujours interprétée de la meme manière selon les gens qui remplissent la carte
-- surtout quand ils ne font pas toujours la différence entre une zone boisée pénétrable et impénétrable
-- voir https://wiki.openstreetmap.org/wiki/Forest

coeffsThroughAOI = {
  ['park']                          = 200,
  ['grass']                         = 50,
}

penaltyHighway = {
  ['primary']                       = 1/4, --racine 4ème 
  ['primary_link']                  = 1/4, --racine 4ème
  ['secondary']                     = 1/2, --racine carrée 
  ['secondary_link']                = 1/2, --racine carrée 
}


coeffsHighway = {
  ['primary']                       = 0, 
  ['primary_link']                  = 0, 
  ['secondary']                     = 0, 
  ['secondary_link']                = 0, 
  ['tertiary']                      = 20,
  ['tertiary_link']                 = 20,
  ['road']                          = 30,
  ['service']                       = 50,
  ['track']                         = 50,
  ['steps']                         = 35,
  ['unclassified']                  = 100,
  ['path']                          = 100,
  ['pedestrian']                    = 100,
  ['footway']                       = 100,
  ['residential']                   = 100,
  ['living_street']                 = 100,
 
}

coeffs = {
  coeffsPOI, 
  coeffsAroundAOI, 
  coeffsThroughAOI, 
  coeffsHighway, 
  penaltyHighway,
  POITable,
  POIBuffer
}

return coeffs