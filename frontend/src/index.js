'use strict';

var L = require('leaflet');
var Geocoder = require('leaflet-control-geocoder');
var LRM = require('leaflet-routing-machine');
var locate = require('leaflet.locatecontrol');
var options = require('./lrm_options');
var links = require('./links');
var leafletOptions = require('./leaflet_options');
var ls = require('local-storage');
var tools = require('./tools');
var state = require('./state');
var localization = require('./localization');
require('./polyfill');
var customControl = require('./control');

var parsedOptions = links.parse(window.location.search.slice(1));
var mergedOptions = L.extend(leafletOptions.defaultState, parsedOptions);
var local = localization.get(mergedOptions.language);

// load only after language was chosen
var itineraryBuilder = require('./itinerary_builder')(mergedOptions.language);

var mapLayer = leafletOptions.layer;
var overlay = leafletOptions.overlay;
var baselayer = ls.get('layer') ? mapLayer[0][ls.get('layer')] : leafletOptions.defaultState.layer;
var layers = ls.get('getOverlay') && [baselayer, overlay['Small Components']] || baselayer;
var poisLayerGroup = new L.LayerGroup();
var poisSecondaryLayerGroup = new L.LayerGroup();
L.Icon.Default.imagePath = './images';
var map = L.map('map', {
  zoomControl: true,
  dragging: true,
  layers: layers,
  maxZoom: 18
}).setView(mergedOptions.center, mergedOptions.zoom);

// Pass basemap layers
mapLayer = mapLayer.reduce(function(title, layer) {
  title[layer.label] = L.tileLayer(layer.tileLayer, {
    id: layer.label
  });
  return title;
});

/* Leaflet Controls */
L.control.layers(mapLayer, overlay, {
  position: 'bottomleft'
}).addTo(map);

L.control.scale().addTo(map);

/* Store User preferences */
// store baselayer changes
map.on('baselayerchange', function(e) {
  ls.set('layer', e.name);
});
// store overlay add or remove
map.on('overlayadd', function(e) {
  ls.set('getOverlay', true);
});
map.on('overlayremove', function(e) {
  ls.set('getOverlay', false);
});

/* OSRM setup */
var ReversablePlan = L.Routing.Plan.extend({
  createGeocoders: function() {
    var container = L.Routing.Plan.prototype.createGeocoders.call(this);
    return container;
  }
});

/* Setup markers */
function makeIcon(i, n) {
  var url = 'images/marker-via-icon-2x.png';
  var markerList = ['images/marker-start-icon-2x.png', 'images/marker-end-icon-2x.png'];
  if (i === 0) {
    return L.icon({
      iconUrl: markerList[0],
      iconSize: [20, 56],
      iconAnchor: [10, 28]
    });
  }
  if (i === n - 1) {
    return L.icon({
      iconUrl: markerList[1],
      iconSize: [20, 56],
      iconAnchor: [10, 28]
    });
  } else {
    return L.icon({
      iconUrl: url,
      iconSize: [20, 56],
      iconAnchor: [10, 28]
    });
  }
}


// Get my location control
/*
L.control.locate({
  follow: false,
  setView: true,
  remainActive: false,
  keepCurrentZoomLevel: true,
  stopFollowingOnDrag: false,
  onLocationError: function(err) {
    alert(err.message)
  },
  onLocationOutsideMapBounds: function(context) {
    alert(context.options.strings.outsideMapBoundsMsg);
  },
  showPopup: false,
  locateOptions: {}
}).addTo(map);
*/


/* Routing controls */

var plan = new ReversablePlan([], {
  addWaypoints: false,  
  waypointMode: 'snap',
  position: 'topleft',
  useZoomParameter: options.lrm.useZoomParameter,
  reverseWaypoints: false, 
  // allow dragging
  draggableWaypoints: false,
  dragStyles: options.lrm.dragStyles, //false
  createMarker: function(i, wp, n) {
    var options = {
      draggable: this.draggableWaypoints,
      icon: makeIcon(i, n)
    };
    var marker = L.marker(wp.latLng, options);
    //marker.on('click', function() {
    //  plan.spliceWaypoints(i, 1);
    //});
    return marker;
  },
  // calculate routing while dragging
  routeWhileDragging: false, //options.lrm.routeWhileDragging
  routeDragInterval: options.lrm.routeDragInterval, //false
  // activate geocoder
  /*
  geocoder: Geocoder.nominatim(),
  geocodersClassName: options.lrm.geocodersClassName,
  geocoderPlaceholder: function(i, n) {
    var startend = [local['Start - press enter to drop marker'], local['End - press enter to drop marker']];
    var via = [local['Via point - press enter to drop marker']];
    if (i === 0) {
      return startend[0];
    }
    if (i === (n - 1)) {
      return startend[1];
    } else {
      return via;
    }
  }
  */
  
});

L.extend(L.Routing, itineraryBuilder);

/* Routing options */
var controlOptions = {
  plan: plan,
  routeWhileDragging: options.lrm.routeWhileDragging,
  lineOptions: options.lrm.lineOptions,
  altLineOptions: options.lrm.altLineOptions,
  summaryTemplate: options.lrm.summaryTemplate,
  containerClassName: options.lrm.containerClassName,
  alternativeClassName: options.lrm.alternativeClassName,
  stepClassName: options.lrm.stepClassName,
  language: leafletOptions.defaultState.language, // we are injecting own translations via osrm-text-instructions
  showAlternatives: options.lrm.showAlternatives,
  units: mergedOptions.units,
  serviceUrl: leafletOptions.services[0].path, 
  apikey: leafletOptions.services[0].apikey, 
  poi: parsedOptions.poi == 'true' ,
  profil: parsedOptions.profil ||'',
  useZoomParameter: options.lrm.useZoomParameter,
  routeDragInterval: options.lrm.routeDragInterval,
  collapsible: options.lrm.collapsible
};
var router = (new L.Routing.OSRMv1(controlOptions));
router._convertRouteOriginal = router._convertRoute;

router.buildRouteUrlOriginal = router.buildRouteUrl;
router.buildRouteUrl = function(waypoints, options) {
  // monkey-patch L.Routing.OSRMv1 until it's easier to overwrite with a hook
  var serviceUrl = this.buildRouteUrlOriginal(waypoints, options);
  serviceUrl = serviceUrl + (controlOptions.poi ? '&poi=true' : '&poi=false');
  if (controlOptions.profil !== '') { 
    serviceUrl = serviceUrl + '&profil=' + controlOptions.profil ;
  }
  serviceUrl = serviceUrl + '&apikey=' + controlOptions.apikey ;
  return serviceUrl;
}

router._convertRoute = function(responseRoute) {
  // monkey-patch L.Routing.OSRMv1 until it's easier to overwrite with a hook
  var resp = this._convertRouteOriginal(responseRoute);

  // add pois here
  if (responseRoute.pois && responseRoute.pois.length) {
    resp.pois = responseRoute.pois;
  }

  // deal with instructions
  if (resp.instructions && resp.instructions.length) {
    var i = 0;
    responseRoute.legs.forEach(function(leg) {
      leg.steps.forEach(function(step) {
        // abusing the text property to save the original osrm step
        // for later use in the itnerary builder
        resp.instructions[i].text = step;
        i++;
      });
    });
  };

  return resp;
};



//var lrmControl = L.Routing.control(Object.assign(controlOptions, {
//  router: router
//})).addTo(map);

var lrmControl = new customControl(Object.assign(controlOptions, {
  router: router
})).addTo(map);
var toolsControl = tools.control(localization.get(mergedOptions.language), localization.getLanguages(), options.tools).addTo(map);
var state = state(map, lrmControl, toolsControl, mergedOptions);

lrmControl.on('routingstart', function(e) {
	console.log(e)
});

lrmControl.on('routingerror', function(e) {
	console.log(e)
});

lrmControl.on('routesfound', function(e) {
  
  //select the chosen route
  try {
    var container = document.getElementsByClassName('leaflet-routing-alternatives-container')[0];
    if (container.children.length > mergedOptions.alternative) {
      container.children[mergedOptions.alternative].click();
    }
  }
  catch (err) {
    console.log(err);
  }

  poisLayerGroup.addTo(map);
  poisSecondaryLayerGroup.addTo(map);
});

plan.on('waypointgeocoded', function(e) {
  if (plan._waypoints.filter(function(wp) { return !!wp.latLng; }).length < 2) {
    map.panTo(e.waypoint.latLng);
  }
});

// center map on popup open
map.on('popupopen', function(e) {
  var px = map.project(e.target._popup._latlng); // find the pixel location on the map where the popup anchor is
  px.y -= e.target._popup._container.clientHeight*2/3; // find the height of the popup container, divide by 2, subtract from the Y axis of marker location
  map.panTo(map.unproject(px),{animate: true}); // pan to new center
});

// add onClick => create waypoint
//map.on('click', function (e){
//  addWaypoint(e.latlng);
//});

function addWaypoint(waypoint) {
  var length = lrmControl.getWaypoints().filter(function(pnt) {
    return pnt.latLng;
  });
  length = length.length;
  if (!length) {
    lrmControl.spliceWaypoints(0, 1, waypoint);
  } else {
    if (length === 1) length = length + 1;
    lrmControl.spliceWaypoints(length - 1, 1, waypoint);
  }
}

// User selected routes
lrmControl.on('alternateChosen', function(e) {
  var directions = document.querySelectorAll('.leaflet-routing-alt');
  if (directions[0].style.display != 'none') {
    directions[0].style.display = 'none';
    directions[1].style.display = 'block';
  } else {
    directions[0].style.display = 'block';
    directions[1].style.display = 'none';
  }
});

function displayPath(route){
  var routeGeoJSON = {
    type: 'Feature',
    properties: {
      name: route.name,
      copyright: {
        author: 'OpenStreetMap contributors',
        license: 'http://www.openstreetmap.org/copyright'
      },
      link: {
        href: window.document.location.href,
        text: window.document.title
      },
      time: (new Date()).toISOString()
    },
    geometry: {
      type: 'LineString',
      coordinates: (route.coordinates || []).map(function (coordinate) {
        return [coordinate.lng, coordinate.lat];
      })
    }
  };
  toolsControl.setRouteGeoJSON(routeGeoJSON);
}

function displayPOIs(route){
  
  poisLayerGroup.clearLayers();
  poisSecondaryLayerGroup.clearLayers();
  if (!route.pois) {
    return;
  }
  
  var pois = {
    "type": "FeatureCollection",
    "features":route.pois
  };
  
  var tagLayer =  L.geoJson(pois.features.filter(function(p) {return p.properties.clickable}), {
    pointToLayer: function(feature, latlng) {
      return L.marker(latlng, {icon: createIcon(feature) });
    },
    onEachFeature: function (feature, layer) {
      var popupContent;
      
        // title
        popupContent = "<p><strong>"+ feature.properties.label.fr + "</strong>";
        // main picture
        var images = feature.properties.properties.filter(function(p) { 
          return p.type === 'image' && p.value !== null
        })        
        .sort(function(a,b) {return (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0) });    
        if (images.length>0) {
          if (typeof images[0].value === 'string') {
            popupContent += "<br><img style='height: 100%; width: 100%; object-fit: contain' src=" + images[0].value + "><br/>";
          } else if (typeof images === 'object') {
            popupContent += "<br><img style='height: 100%; width: 100%; object-fit: contain' src=" + images[0].value[0] + "><br/>";
          }
        }
        // all text props
        popupContent += feature.properties.properties.filter(function(p) { 
          return p.type === 'text' && p.value !== null
        })
        .sort(function(a,b) {return (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0) })    
        .map(function(p) {
          if (p.label.fr === 'description') return "<br/><strong>" + p.label.fr + '</strong> : ' + p.value +"</p><p>";
          if (isValidDate(new Date(p.value))) {p.value = new Date(p.value).toLocaleDateString()}
          return "<br/><strong>" + p.label.fr + '</strong> : ' + p.value ;
        })
        // links
        if (hasLink(feature)) {
          popupContent += "<br/><br/> En savoir plus:"
          popupContent += feature.properties.properties.filter(function(p) { 
            return p.type === 'link' && p.value !== null
          })
          .sort(function(a,b) {return (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0) })  
          .map(function(p) { 
            // JCS: remove when data clean
            if (!p.value.includes('https://')) return "<br/><a target='_blank' href='https://" + p.value + "'>" + p.label.fr + "</a>";          
            return "<br/><a target='_blank' href=" + p.value + ">" + p.label.fr + "</a>";
          }) 
        }
      popupContent += "</p>";
      popupContent = popupContent.replace('<p>,<br/>','<p>')
      layer.bindPopup(popupContent);
    }
  });

  var genericTagLayer =  L.geoJson(pois.features.filter(function(p) {return !p.properties.clickable}), {
    pointToLayer: function(feature, latlng) {
      return L.marker(latlng, {icon: createIcon(feature) });
    }
  })     
  if (genericTagLayer) {
    poisSecondaryLayerGroup.addLayer(genericTagLayer);
  }
  if (tagLayer){
    poisLayerGroup.addLayer(tagLayer);
  }
}

function isValidDate(d) {
  return d instanceof Date && !isNaN(d);
}

function createIcon(feature) {
  return L.icon({            
    iconUrl: 'images/' + feature.properties.icon || 'images/marker-via-icon-2x.png',
    iconSize: feature.properties.iconSize ? feature.properties.iconSize : [20, 56],
    iconAnchor: feature.properties.iconSize ? [feature.properties.iconSize[0]/2,feature.properties.iconSize[1]*5/7] : [10, 40],
  });
}

/* Leaflet Controls */

L.control.layers(mapLayer, {
  "Points d'intérêts":poisLayerGroup,
  'Information':poisSecondaryLayerGroup
}, {
  position: 'bottomleft'
}).addTo(map);

L.control.scale().addTo(map);


function hasLink(feature) {
  return feature.properties.properties.find(function(p) { 
    return p.type === 'link' && p.value !== null
  })
}

lrmControl.on('routeselected', function(e) {
  var route = e.route || {};
  displayPath(route);
  if (controlOptions.poi) {
    displayPOIs(route);
  }
});
plan.on('waypointschanged', function(e) {
  if (!e.waypoints ||
      e.waypoints.filter(function(wp) { return !wp.latLng; }).length > 0) {
    toolsControl.setRouteGeoJSON(null);
  }
});

