*** COMMENT INTEGRER FOXTROT DANS UN SERVICE ? ***
* Pour comprendre Foxtrot, possible de voir un exemple de rendu carto sur `http://foxtrot.eneo.fr/route/?loc=1.909132,47.900642&loc=1.914289,47.916479&alt=0&poi=true&profil=osrm-zen-foot`
* Chaque développeur possède une clé d'API 'api-key' unique pour accéder au service. La variable api-key doit être renseignée dans le header de la requête.
* url principale : `http://foxtrot.eneo.fr/v1/route/v1/driving/`
* paramètres obligatoires :
coordonnées dans cet ordre : start_point_lon,start_point_lat;end_point_lon,end_point_lat
* paramètres optionnels :
`alternatives`: _booléen_, si _true_ retourne une route alternative si elle est trouvée. défaut _true_.
`steps`: _booléen_, si _true_ retourne les informations d'itinéraire à suivre. défaut _true_.
`poi`: _booléen_, si _true_ retourne des points d'interet provenant de données open data du territoire. défaut _false_.
`profil`: osrm-green-foot/osrm-zen-foot, choose an available profile "green" or "zen". défaut _osrm-green-foot_
* exemple d'un appel à l'API avec votre clé d'API
`curl --location --request GET 'http://foxtrot.eneo.fr/v1/route/v1/driving/1.909132,47.900642;1.914289,47.916479?alternatives=0&steps=true&poi=true&profil=osrm-zen-foot' --header 'api-key: xxxxxx'`