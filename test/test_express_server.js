const http = require('http')

const data = JSON.stringify({
    coordinates: [[1.908607, 47.902734], [1.920888, 47.930916]],
    alternatives: true,
    origin: "chatbot"
})

const options = {
  hostname: 'localhost',
  port: 80,
  path: '/route',
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'Content-Length': data.length
  }
}

const req = http.request(options, res => {
  res.setEncoding('utf8');
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', d => {
    console.log(d)
  })
})

req.on('error', error => {
  console.error(error)
})

req.write(data)
req.end()